# CS 373 Assignment 2

https://gitlab.com/asnelling/cs-373-assignment-2

Students

- [Addison Snelling](https://gitlab.com/asnelling) (EID: as62865)
- [Megan Zhao](https://gitlab.com/banye0913)

## Bob

### 2.1

```Shell
$ git clone git@gitlab.com:asnelling/cs-373-assignment-2
```

### 2.4

Git recognizes the new file, but we have not yet told git to track
changes to the file.

```Shell
(git)-[master]-% git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	myfile.txt

nothing added to commit but untracked files present (use "git add" to track)
```

### 2.5, 2.6

```
(git)-[master]-% git add myfile.txt
(git)-[master]-% git commit -m "first commit (Bob)"
[master (root-commit) bc1a928] first commit (Bob)
 1 file changed, 14 insertions(+)
 create mode 100644 myfile.txt
```

### 2.7, 2.8

This tells us two things:

1. our checkout is consistent with git's local index (there are no untracked modifications), and
2. our local copy of `master` branch is consistent with remote branch in
GitLab.

```
(git)-[master]-% git status
On branch master
nothing to commit, working tree clean
```


<!-- vim: set ft=markdown :-->